from json import dumps
from re import split
from flask import Flask, request
app = Flask(__name__)

@app.route('/')
def hello_world():
    user_agent = request.headers.get('User-Agent')
    data = split(r"\(|\)", user_agent)
    browser = data[-1]
    platform = data[1]
    if not request.headers.getlist("X-Forwarded-For"):
        ip = request.remote_addr
    else:
        ip = request.headers.getlist("X-Forwarded-For")[0]

    datafied = {
        "browser": browser,
        "platform": platform,
        "user_ip": ip
    }
    return dumps(datafied, indent=4)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
